<?php 
    namespace data;

    // Класс купонов
    class Coupons extends \libs\Base
    {
		// Массив конфигурации
		protected $config;

		// Общее количество купонов
		protected $count = 0;
		// Количество купонов, выводимых на страницу
		protected $limit = 10;
		// Текущая страница купонов, отсчитывая с 0
		protected $page = 1;

		// Список ошибок
		protected $errors;

		// Шаблон страницы
		protected $template;
		// Отображать ли шаблон?
		protected $show = true;

		// Текущий обработчик
		protected $action = null;
		// Код поиска
		protected $code = null;


		public function __construct(array $config)
		{
			$this->config = $config;
			parent::__construct($this->config['db_host'], $this->config['db_name'], $this->config['db_user'], $this->config['db_password']);

			$this->errors = new \libs\Errors;
			$this->template = new \libs\Template;
		}

		// Инициализация класса
		public function init()
		{	
			$this->router();

			if ($this->show)
			{
				$this->prepareData();
				$this->template->show();
			}
		}

		
		
		// Подготовка данных для отображения в шаблоне
		protected function prepareData()
		{
			// Текущий год
			$this->template->addParam('year', date('Y'));
			$this->template->addParam('action', $this->action);
			$this->template->addParam('code', $this->code);
		}

	
		// Отвечает за общий вывод купонов
		protected function pageAction()
		{
			$this->template->addParam('coupons', $this->getCoupons());
			$this->setCouponsCount();
			$this->template->setPaginator(ceil($this->count / $this->limit), $this->page, '/page/');
		}
		
		// Поиск купонов
		protected function searchAction()
		{
			// Если код не установлен, то и нечего искать
			if (!$this->code)
			{
				$this->template->addParam('coupons', null);
				return false;
			}
			
			// Получаем список купонов
			$coupons = $this->getCoupons($this->code);
			
			// Подготавливаем полученный список к выводу на страницу
			foreach($coupons as &$coupon)
			{
				$coupon['code'] = str_replace($this->code, '<mark>'.$this->code.'</mark>', $coupon['code']);
			}
			
			$this->template->addParam('coupons', $coupons);
			
			$this->setCouponsCount();
			$this->template->setPaginator(ceil($this->count / $this->limit), $this->page, '/search/'.$this->code.'/');
		}
		
		// Активация
		protected function activationAction(bool $json = false)
		{
			$id = $this->checkId();
			
			// Завершить обработчик, если id пуст
			if (!$id)
			{
				return false;
			}

			// Пытаемся активировать купон
			$this->activation($id);
			
			if ($json)
			{
				// Отменяем отображение шаблона
				$this->show = false;
				// Возвращаем дату активации и упаковываем дату в json
				echo json_encode($this->returnDate($id));
			}
			else
			{
				$this->redirect();
			}
		}
		
		// Получить купоны в количестве $limit со страницы $page совпадающие с $code
		protected function getCoupons()
		{
			/// Настраиваем параметры поиска
			$search_code = '';
			$like = '';
			$this->setSearch($search_code, $like);
			
			// Номер последнего элемента, возвращаемого запросом. Вычитаем $limit, чтобы в uri номер страницы начинался с единицы
			$row_number = $this->page * $this->limit - $this->limit;

			$request = self::$db->prepare('SELECT id_coupon AS id, code, date FROM coupons'.$like.' LIMIT :page, :limit');
			$request->bindParam(':page', $row_number, \PDO::PARAM_INT);
			$request->bindParam(':limit', $this->limit, \PDO::PARAM_INT);
			if ($this->code)
			{
				$request->bindParam(':code', $search_code, \PDO::PARAM_STR);
			}
			$request->execute();
			
			return $request->fetchAll();
		}
		
		// Установить количество элементов в текущем запросе
		protected function setCouponsCount()
		{
			/// Настраиваем параметры поиска
			$search_code = '';
			$like = '';
			$this->setSearch($search_code, $like);

			// Возвращаем количество элементов в запросе
			$request_count = self::$db->prepare('SELECT COUNT(*) AS count FROM coupons'.$like);
			if ($this->code)
			{
				$request_count->bindParam(':code', $search_code, \PDO::PARAM_STR);
			}
			$request_count->execute();
			
			$this->count = (int)$request_count->fetch()['count'];
		}

		
		
		// Роутер
		private function router()
		{
			$uri_parser = new \libs\uriParser;

			// Устанавливаем обработчик поумолчанию
			$this->setAction($uri_parser, 0);

			// Подключаем нужный экшн
			switch ($this->action)
			{
				case 'page':
					$this->setPage($uri_parser, 1);
					$this->pageAction();
					break;
				case 'search':
					// Выводим значение $code в строку браузера
					$code = filter_input(INPUT_POST, 'code');
					
					if ($code && is_string($code))
					{
						header('Location: /search/'.$code);
					}
					
					// Запускаем обработчик поиска
					$this->setCode($uri_parser, 1);
					$this->setPage($uri_parser, 2);
					$this->searchAction();
					break;
				case 'activate':
					// Следует ли вернуть данные в json формате
					$json = filter_input(INPUT_POST, 'json');

					if (is_string($json) && preg_match('/^true$/i', $json))
					{
						$this->activationAction(true);
					}
					else
					{
						$this->activationAction();
					}
					break;
				default:
					$this->pageAction();
			}
		}
		
		// Установить параметр "обработчик"
		private function setAction(\libs\uriParser $uri_parser, int $number)
		{
			// Парсим начальное значение, в зависимости от него будет подключаться нужный обработчик
			$action_uri = $uri_parser->getParam($number);

			if (preg_match('/^[a-z]{3,}$/i', $action_uri))
			{
				$this->action = $action_uri;
			}
		}
		
		// Установить параметр "номер страницы"
		private function setPage(\libs\uriParser $uri_parser, int $number)
		{
			$page_uri = $uri_parser->getParam($number);

			if (preg_match('/^[0-9]+$/', $page_uri))
			{
				$this->page = (int)$page_uri;
			}
		}
		
		// Установить параметр "код поиска"
		private function setCode(\libs\uriParser $uri_parser, int $number)
		{
			$code = $uri_parser->getParam($number);

			if (preg_match('/^[0-9]{1,12}$/', $code))
			{
				$this->code = $code;
			}
		}

		
		private function setSearch(string &$search_code, string &$like)
		{
			if ($this->code)
			{
				$search_code = '%'.$this->code.'%';
				$like = ' WHERE code LIKE :code';
			}
			else
			{
				$search_code = null;
				$like = null;
			}
		}
		
		
		// Активирует купон в введеным id
		private function activation($id)
		{
			$request = self::$db->prepare('UPDATE coupons SET date = CURRENT_TIMESTAMP WHERE id_coupon = :id');
			$request->bindParam(':id', $id, \PDO::PARAM_INT);
			$request->execute();
		}
		
		// Проверяем id, переданный post запросом на соответствие шаблону
		private function checkId()
		{
			$validation = new \libs\Validation();
			$validation->validate('id', '/^[0-9]+$/');
			
			if ($validation->getErrors()->getCount())
			{
				return null;
			}
			else
			{
				return $validation->getValidated()['id'];
			}
		}
		
		
		// Возвращаем дату активации купона
		private function returnDate($id)
		{
			$request = self::$db->prepare('SELECT date FROM coupons WHERE id_coupon = :id');
			$request->bindParam(':id', $id, \PDO::PARAM_INT);
			$request->execute();
			
			return $request->fetch()['date'];
		}
		
		
		// Функция переадресация на страницу, с которой был совершен переход
		private function redirect()
		{
			// Выполняем переадресацию на страницу, с которой был отправлен запрос на активацию
			$from_uri = filter_input(INPUT_SERVER, 'HTTP_REFERER');
			if ($from_uri)
			{
				$location = $from_uri;
			}
			else
			{
				$location = '/';
			}

			header('Location: '.$location);
		}
    }
