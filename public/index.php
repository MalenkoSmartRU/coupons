<?php
    ini_set('display_errors', 'on');
    error_reporting(E_ALL);
    mb_internal_encoding('UTF-8');

    try
    {
		// Автозагрузка классов
		spl_autoload_register(function($class_name)
		{
			require_once str_replace('\\', '/', $class_name) . '.class.php';
		});

		// Новый объект "Купоны"
		(new data\Coupons(require_once \Cst::ROOT . 'config.php'))->init();
    }
    catch (PDOException $e)
    {
		exit('Ошибка подключения к базе данных');
    }