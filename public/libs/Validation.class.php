<?php
	namespace libs;
	
	// Класс валидации входных данных типа string
	class Validation
	{
		// Входящие данные
		protected $request;
		
		// Ошибки
		protected $errors;
		
		// Значения, прошедшие валидацию
		protected $validated = [];
		
		public function __construct($type = INPUT_POST)
		{
			$this->request = filter_input_array($type);
			$this->errors = new Errors;
		}
		
		public function validate(string $key, string $pattern, string $error_message = '',  int $max_length = null, int $min_length = 0)
		{
			// Если запроса не существует
			if (!$this->request)
			{
				return null;
			}
			
			// Проверяем на существование ключ во входящем массиве
			if (!array_key_exists($key, $this->request))
			{
				$this->errors->add('Не был отправлен параметр ' . $key . ', перезагрузите страницу и попробуйте снова');
				
				return null;
			}
			
			// Валидация
			if (is_string($this->request[$key])
				&& preg_match("/^.{".$min_length.",".$max_length."}$/usm", $this->request[$key])
				&& preg_match($pattern, $this->request[$key]))
			{
				$this->validated[$key] = $this->request[$key];
				
				return true;
			}
			else
			{
				$this->errors->add($error_message);
				
				return false;
			}
		}

		
		// Получить значения
		public function getRequest()
		{
			return $this->request;
		}
		
		public function getValidated()
		{
			return $this->validated;
		}
		
		public function getErrors()
		{
			return $this->errors;
		}
	}
