<?php
	namespace libs;

	// Класс описывающий шаблоны
	class Template
	{
		protected $data = [];
		
		protected $paginator = null;

		// Показать шаблон пользователю
		public function show()
		{
			$data = &$this->data;
			
			require_once \Cst::PATH_TPL . 'index.tpl';
		}
		
		// Добавить новые параметры для отображения в шаблоне
		public function addParam($name, $param)
		{
			if (isset($this->data[$name])) 
			{
				return false;
			}
			else
			{
				$this->data[$name] = $param;
				return true;
			}
		}
		
		public function setPaginator($count, $current, $uri)
		{
			$this->paginator = new \libs\Paginator($count, $current, $uri);
		}
	}
