<?php
	namespace libs;
	
	// Обрабатывает текущую строку запроса
	class uriParser
	{
		private $uri;
		// Минимальная длина одного элемента
		private $min_len = 1;
		// Количество возвращаемых частей
		private $limit = -1;
		// Массив частей
		private $parameters;

		public function __construct() {
			$this->uri = filter_input(INPUT_SERVER, 'REQUEST_URI');
			$this->checkUri();
			$this->explodeUri();
		}
		
		// Возвращает параметр по его номеру
		public function getParam(int $number)
		{
			if (isset($this->parameters[$number]))
			{
				return $this->parameters[$number];
			}
			else
			{
				return null;
			}
		}
		
		// Проверяет текущий uri на соответствие шаблону (/uri ... /abc)
		private function checkUri()
		{
			if (!preg_match('/^(\/|\/[a-z0-9]{'.$this->min_len.',})+$/i', $this->uri))
			{
				$this->uri = null;
			}
		}
		
		// Делит uri на элементы массива
		private function explodeUri()
		{
			
			if (!is_null($this->uri))
			{
				$this->parameters = preg_split("/[\/]+/", $this->uri, $this->limit, PREG_SPLIT_NO_EMPTY);
			}
		}
	}

