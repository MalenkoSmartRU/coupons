<?php
	namespace libs;
	
	// Основа для всех классов, которые будут использовать базу данных
	class Base
	{
		protected static $db;
		
		public function __construct($db_host = 'mysql', $db_name = 'test', $db_user = 'root', $db_password = 'root')
		{
			// Подключение к базе данных
			self::$db = new \PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_user, $db_password);
			self::$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
		}
	}