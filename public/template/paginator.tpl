<ul class="main-paginator">
	<?php if($this->count > 6):?>
		<?php if(($this->current >= 3) && ($this->current <= ($this->count - 2))): ?>
			<li><a href="<?=$this->uri?>1">1</a></li>
			<li class="dotter">...</li>
			<li><a href="<?=$this->uri?><?=$this->current-1?>"><?=$this->current-1?></a></li>
			<li class="current"><?=$this->current?></li>
			<li><a href="<?=$this->uri?><?=$this->current+1?>"><?=$this->current+1?></a></li>
			<li class="dotter">...</li>
			<li><a href="<?=$this->uri?><?=$this->count?>"><?=$this->count?></a></li>
		<?php elseif(($this->current === 1)): ?>
			<li class="current">1</li>
			<li><a href="<?=$this->uri?><?=$this->current+1?>"><?=$this->current+1?></a></li>
			<li><a href="<?=$this->uri?><?=$this->current+2?>"><?=$this->current+2?></a></li>
			<li class="dotter">...</li>
				<li><a href="<?=$this->uri?><?=$this->count?>"><?=$this->count?></a></li>
		<?php elseif(($this->current === 2)): ?>
			<li><a href="<?=$this->uri?>1">1</a></li>
			<li class="current">2</li>
			<li><a href="<?=$this->uri?><?=$this->current+1?>"><?=$this->current+1?></a></li>
			<li><a href="<?=$this->uri?><?=$this->current+2?>"><?=$this->current+2?></a></li>
			<li class="dotter">...</li>
			<li><a href="<?=$this->uri?><?=$this->count?>"><?=$this->count?></a></li>
		<?php elseif(($this->current === $this->count)): ?>
			<li><a href="<?=$this->uri?>1">1</a></li>
			<li class="dotter">...</li>
			<li><a href="<?=$this->uri?><?=$this->current-2?>"><?=$this->current-2?></a></li>
			<li><a href="<?=$this->uri?><?=$this->current-1?>"><?=$this->current-1?></a></li>
			<li class="current"><?=$this->count?></li>
		<?php elseif(($this->current === ($this->count - 1))): ?>
			<li><a href="<?=$this->uri?>1">1</a></li>
			<li class="dotter">...</li>
			<li><a href="<?=$this->uri?><?=$this->current-2?>"><?=$this->current-2?></a></li>
			<li><a href="<?=$this->uri?><?=$this->current-1?>"><?=$this->current-1?></a></li>
			<li class="current"><?=$this->count-1?></li>
			<li><a href="<?=$this->uri?><?=$this->count?>"><?=$this->count?></a></li>
		<?php endif ?>
	<?php else: ?>
		<?php for($i = 1; $i <= $this->count; $i++): ?>
			<li<?php if ($this->current === $i): ?> class="current"<?php endif; ?>><?php if ($this->current === $i): ?><?=$i?><?php else: ?><a href="<?=$this->uri?><?=$i?>"><?=$i?></a><?php endif ?></li>
		<?php endfor ?>
	<?php endif ?>
</ul>