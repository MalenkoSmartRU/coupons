// Реализует активацию купонов без перезагрузки страницы
$(function() {
	// Предотвратить отправку формы
	$('#activate').on('submit', function(event) {
		event.preventDefault();
	});

	// Обработать событие клика по кнопке
	$('#activate .activate').on('click', function() {
		var value = $(this).val();
		var thisbutton = $(this);
		
		$.ajax({
			url: '/activate/',
			method: 'post',
			data: 'json=true&id=' + value,
			success: function(data) {
				var date = $.parseJSON(data);
				
				thisbutton.parent('td').html(date).parent('tr').find('.status').html('Активирован');
			}
		});
	});
});