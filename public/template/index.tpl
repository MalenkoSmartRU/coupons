<html>
	<head>
		<title>Список купонов</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="/template/css/reset.css">
		<link rel="stylesheet" type="text/css" href="/template/css/style.css">
		
		<script src="/template/js/jquery-3.3.1.min.js"></script>
		<script src="/template/js/activate.js"></script>
	</head>
	<body>
		<div id="test"></div>
		<header class="main-header">
			<div class="content">
				<div class="block">
					<form method="post" action="/search/" class="main-search">
						<input placeholder="Код купона" name="code" class="code">
						<input type="submit" value="Найти" name="submit" class="submit">
					</form>
				</div>
			</div>
		</header>
		<main class="main-content">
			<div class="content">
				<div class="block">
					<?php if ($data['action'] === 'search'): ?>
						<nav class="main-nav">
							<a href="/">Показать весь список</a>
						</nav>
					<?php endif; ?>
					<?php if($this->data['coupons']): ?>
						<form method="post" action="/activate/" id="activate">
							<input type="hidden" name="json" value="true">
							<table class="main-coupons">
								<thead>
									<tr class="header">
										<th class="code">Код купона</th>
										<th class="status">Статус</th>
										<th class="date">Дата активации</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($data['coupons'] as $coupon):?>
										<tr>
											<td class="code"><strong><?=$coupon['code']?></strong></td>
											<td class="status"><?php if($coupon['date']):?>Активирован<?php else:?>Не активирован<?php endif;?></td>
											<td class="date"><?php if($coupon['date']): ?><?=$coupon['date']?><?php else: ?><button type="submit" name="id" value="<?=$coupon['id']?>" class="activate">Активировать</button><?php endif; ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</form>
						<?=$this->paginator->show()?>
					<?php else: ?>
						<div>Поиск не дал результатов</div>
					<?php endif; ?>
				</div>
			</div>
		</main>
		<footer class="main-footer">
			<div class="content">
				<div class="block">
					<?=$data['year']?>
				</div>
			</div>
		</footer>
	</body>
</html>